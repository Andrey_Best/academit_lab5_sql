Business DataBase Editor v. 1.0.0

Very comfortable database editor for supervisor-representative checking business.

Native UI with understandable buttons.
Used Comic Sans font.

To start application open "Lab5.exe" app.

Login in account is made automaticly, by using new mind reading technologies. ╰(•̀ 3 •́)━☆ﾟ.*･｡ﾟ


To add new row in database - use "Add Data" buttons, and fulfill required info.

To get tables from database - use "Table Data" buttons.
Requested data will be shown on the right window of Data Base Grid.

To update information of some rows of database - check "Show Raw Tables" tick, and press one of "Table Data" buttons,
change row(s) with information you need, and then press "Update Data" button.



Made in WPF with C#.

Made with love by Andrey Kotliar <3