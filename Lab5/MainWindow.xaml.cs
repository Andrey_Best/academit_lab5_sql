﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace Lab5
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string loginAndPassword = "u_student";
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            FillLoginAndPassword();
        }

        private async void FillLoginAndPassword()
        {
            textBoxLogin.Focus();
            for (int i = 0; i < loginAndPassword.Length; i++)
            {
                await Task.Delay(200);
                textBoxLogin.Text += loginAndPassword[i];
            }
            passwordBoxPassword.Focus();
            for (int i = 0; i < loginAndPassword.Length; i++)
            {
                await Task.Delay(200);
                passwordBoxPassword.Password += loginAndPassword[i];
            }

            buttonLogin.Focus();
            await Task.Delay(500);
            buttonLogin.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
        }

        private void ButtonLogin_Click(object sender, RoutedEventArgs e)
        {
            new MainMenu().Show();
            Close();
        }
    }
}
