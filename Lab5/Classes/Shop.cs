﻿namespace Lab5
{
    public class Shop
    {
        public readonly string Text;
        public readonly int id;

        public Shop(string name, string address, int id)
        {
            this.id = id;
            Text = name + " - " + address;
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
