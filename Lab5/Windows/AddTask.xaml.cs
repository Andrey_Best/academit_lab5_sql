﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows;

namespace Lab5.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddTask.xaml
    /// </summary>
    public partial class AddTask : Window
    {
        private SqlConnection connection;

        public AddTask(SqlConnection connection)
        {
            this.connection = connection;
            InitializeComponent();
        }

        private void WindowAddTask_Loaded(object sender, RoutedEventArgs e)
        {
            SqlCommand commandSupervisors = new SqlCommand("SELECT users.first_name, users.last_name, supervisors.supervisor_id FROM supervisors JOIN users ON users.user_id = supervisors.user_id", connection);
            SqlCommand commandRepresentatives = new SqlCommand("SELECT users.first_name, users.last_name, representatives.representative_id FROM representatives JOIN users ON users.user_id = representatives.user_id", connection);
            SqlCommand commandShops = new SqlCommand("SELECT shops.shop_id, shops.address, shops.name FROM shops;", connection);
            try
            {
                List<User> usersSupervisors = new List<User>();
                List<User> usersRepresentatives = new List<User>();
                List<Shop> shops = new List<Shop>();

                SqlDataReader readerSupervisors = commandSupervisors.ExecuteReader();

                while (readerSupervisors.Read())
                {
                    usersSupervisors.Add(new User(readerSupervisors["first_name"].ToString(), readerSupervisors["last_name"].ToString(), int.Parse(readerSupervisors["supervisor_id"].ToString())));
                }
                readerSupervisors.Close();

                SqlDataReader readerRepresentatives = commandRepresentatives.ExecuteReader();

                while (readerRepresentatives.Read())
                {
                    usersRepresentatives.Add(new User(readerRepresentatives["first_name"].ToString(), readerRepresentatives["last_name"].ToString(), int.Parse(readerRepresentatives["representative_id"].ToString())));
                }
                readerRepresentatives.Close();

                SqlDataReader readerShops = commandShops.ExecuteReader();

                while (readerShops.Read())
                {
                    shops.Add(new Shop(readerShops["name"].ToString(), readerShops["address"].ToString(), int.Parse(readerShops["shop_id"].ToString())));
                }
                readerShops.Close();

                for (int i = 0; i < usersSupervisors.Count; i++)
                {
                    comboBoxSuperVisors.Items.Add(usersSupervisors[i]);
                }
                if (usersSupervisors.Count > 0)
                {
                    comboBoxSuperVisors.SelectedIndex = 0;
                }

                for (int i = 0; i < usersRepresentatives.Count; i++)
                {
                    comboBoxRepresentatives.Items.Add(usersRepresentatives[i]);
                }
                if (usersRepresentatives.Count > 0)
                {
                    comboBoxRepresentatives.SelectedIndex = 0;
                }

                for (int i = 0; i < shops.Count; i++)
                {
                    comboBoxShops.Items.Add(shops[i]);
                }
                if (shops.Count > 0)
                {
                    comboBoxShops.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonAddTask_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxTopic.Text != "" && textBoxDescription.Text != "" && comboBoxSuperVisors.Text != "" && comboBoxRepresentatives.Text != "" && comboBoxShops.Text != "")
            {
                SqlCommand command = new SqlCommand("INSERT INTO tasks (topic, description, is_important, assigner_id, executor_id, shop_id, complition_comment)" +
                                                    "VALUES (@topic, @description, @isImportant, @assignerId, @executorId, @shopID, @complitionComment);", connection);
                command.Parameters.AddWithValue("@topic", textBoxTopic.Text);
                command.Parameters.AddWithValue("@description", textBoxDescription.Text);
                if (checkBoxIsImportant.IsChecked ?? true)
                {
                    command.Parameters.AddWithValue("@isImportant", 1);
                }
                else if (checkBoxIsImportant.IsChecked ?? false)
                {
                    command.Parameters.AddWithValue("@isImportant", 0);
                }
                else
                {
                    command.Parameters.AddWithValue("@isImportant", 0);
                }
                command.Parameters.AddWithValue("@assignerId", (comboBoxSuperVisors.SelectedItem as User).id);
                command.Parameters.AddWithValue("@executorId", (comboBoxRepresentatives.SelectedItem as User).id);
                command.Parameters.AddWithValue("@shopID", (comboBoxShops.SelectedItem as Shop).id);
                command.Parameters.AddWithValue("@complitionComment", textBoxComplitionComment.Text);
                try
                {
                    command.ExecuteNonQuery();

                    MessageBox.Show(Constants.successfullQueryMessage);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(Constants.fieldfillingWarning);
            }
        }
    }
}
