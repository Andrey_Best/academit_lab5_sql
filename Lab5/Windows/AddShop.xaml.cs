﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows;

namespace Lab5.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddShop.xaml
    /// </summary>
    public partial class AddShop : Window
    {
        private SqlConnection connection;

        public AddShop(SqlConnection connection)
        {
            this.connection = connection;
            InitializeComponent();
        }

        private void WindowAddShop_Loaded(object sender, RoutedEventArgs e)
        {
            SqlCommand command = new SqlCommand("SELECT users.first_name, users.last_name, representatives.representative_id FROM representatives JOIN users ON users.user_id = representatives.user_id", connection);
            try
            {
                List<User> users = new List<User>();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    users.Add(new User(reader["first_name"].ToString(), reader["last_name"].ToString(), int.Parse(reader["representative_id"].ToString())));
                }
                reader.Close();

                for (int i = 0; i < users.Count; i++)
                {
                    comboBoxRepresentatives.Items.Add(users[i]);
                }
                if (users.Count > 0)
                {
                    comboBoxRepresentatives.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonAddShop_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxAddress.Text != "" && textBoxName.Text != "" && comboBoxRepresentatives.Text != "")
            {
                SqlCommand command = new SqlCommand("INSERT INTO shops (address, name, representative_id)" +
                                                    "VALUES (@address, @name, @representativeId);", connection);
                command.Parameters.AddWithValue("@address", textBoxAddress.Text);
                command.Parameters.AddWithValue("@name", textBoxName.Text);
                command.Parameters.AddWithValue("@representativeId", (comboBoxRepresentatives.SelectedItem as User).id);
                try
                {
                    command.ExecuteNonQuery();

                    MessageBox.Show(Constants.successfullQueryMessage);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(Constants.fieldfillingWarning);
            }
        }
    }
}
