﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows;

namespace Lab5.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddUser.xaml
    /// </summary>
    public partial class AddUser : Window
    {
        private SqlConnection connection;
        private string userType;

        public AddUser(SqlConnection connection, string userType)
        {
            this.connection = connection;
            this.userType = userType;
            InitializeComponent();
        }

        private void WindowAddUser_Loaded(object sender, RoutedEventArgs e)
        {
            labelAddUser.Content = "Add " + userType;
            buttonAddUser.Content = "Add " + userType;
            if (userType == "Supervisor")
            {
                comboBoxSuperVisors.Visibility = Visibility.Hidden;
            }
            else
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand("SELECT users.first_name, users.last_name, supervisors.supervisor_id FROM supervisors JOIN users ON users.user_id = supervisors.user_id", connection);
 
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    List<User> users = new List<User>();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        users.Add(new User( reader["first_name"].ToString(), reader["last_name"].ToString(), int.Parse(reader["supervisor_id"].ToString()) ));
                    }
                    reader.Close();

                    for (int i = 0; i < users.Count; i++)
                    {
                        comboBoxSuperVisors.Items.Add(users[i]);
                    }
                    if (users.Count > 0)
                    {
                        comboBoxSuperVisors.SelectedIndex = 0;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ButtonAddUser_Click(object sender, RoutedEventArgs e)
        {
            if (userType == "Representative")
            {
                if (textBoxFirstName.Text != "" && textBoxLastName.Text != "" && textBoxCity.Text != "" && textBoxPhoneNumber.Text != "" && comboBoxSuperVisors.Text != "")
                {
                    SqlCommand command = new SqlCommand("INSERT INTO users (first_name, last_name, city, phone_number)" +
                                                        "VALUES (@firstName, @lastName, @city, @phoneNumber);" +
                                                        "SELECT SCOPE_IDENTITY();", connection);
                    command.Parameters.AddWithValue("@firstName", textBoxFirstName.Text);
                    command.Parameters.AddWithValue("@lastName", textBoxLastName.Text);
                    command.Parameters.AddWithValue("@city", textBoxCity.Text);
                    command.Parameters.AddWithValue("@phoneNumber", textBoxPhoneNumber.Text);
                    try
                    {
                        int userID = int.Parse(command.ExecuteScalar().ToString());

                        SqlCommand commandSupevisor = new SqlCommand("INSERT INTO representatives (user_id, supervisor_id) " +
                                                                        "VALUES (@userID, @supervisorID);", connection);
                        commandSupevisor.Parameters.AddWithValue("@userID", userID);
                        commandSupevisor.Parameters.AddWithValue("@supervisorID", (comboBoxSuperVisors.SelectedItem as User).id);

                        commandSupevisor.ExecuteNonQuery();

                        MessageBox.Show(Constants.successfullQueryMessage);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show(Constants.fieldfillingWarning);
                }
            }
            else if (userType == "Supervisor")
            {
                if (textBoxFirstName.Text != "" && textBoxLastName.Text != "" && textBoxCity.Text != "" && textBoxPhoneNumber.Text != "")
                {
                    SqlCommand command = new SqlCommand("INSERT INTO users (first_name, last_name, city, phone_number)" +
                                                        "VALUES (@firstName, @lastName, @city, @phoneNumber);" +
                                                        "SELECT SCOPE_IDENTITY();", connection);
                    command.Parameters.AddWithValue("@firstName", textBoxFirstName.Text);
                    command.Parameters.AddWithValue("@lastName", textBoxLastName.Text);
                    command.Parameters.AddWithValue("@city", textBoxCity.Text);
                    command.Parameters.AddWithValue("@phoneNumber", textBoxPhoneNumber.Text);
                    try
                    {
                        int userID = int.Parse(command.ExecuteScalar().ToString());

                        SqlCommand commandSupevisor = new SqlCommand("INSERT INTO supervisors (user_id) " +
                                                                        "VALUES (@userID);", connection);
                        commandSupevisor.Parameters.AddWithValue("@userID", userID);

                        commandSupevisor.ExecuteNonQuery();

                        MessageBox.Show(Constants.successfullQueryMessage);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show(Constants.fieldfillingWarning);
                }
            }

        }

    }
}
