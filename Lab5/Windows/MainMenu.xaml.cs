﻿using Lab5.Windows;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows;

namespace Lab5
{
    /// <summary>
    /// Логика взаимодействия для MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Window
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private SqlConnection connection;
        private SqlDataAdapter dataAdapter;
        private DataSet dataSet;
        private string showedTable;
        private bool isRawTable;

        private void WindowMainMenu_Loaded(object sender, RoutedEventArgs e)
        {
            // Open the connection in try/catch block.
            try
            {
                connection = new SqlConnection(Constants.connectionString);
                connection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            CopyIfRawTableCheckBox();
            DrawTableBySQLCommand("SELECT representatives.representative_id, users.first_name, users.last_name, users.city, users.phone_number " +
                                  "FROM representatives JOIN users ON users.user_id = representatives.user_id;", "representatives");
            UpdateSupervisorsComboBox();
            UpdateRepresentativesComboBox();
            UpdateTopicsComboBox();
        }

        private void WindowMainMenu_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.Close();
        }


        private void ButtonAddSupervisor_Click(object sender, RoutedEventArgs e)
        {
            AddUser window = new AddUser(connection, "Supervisor");
            window.Closed += delegate { UpdateSupervisorsComboBox(); };
            window.Show();
        }

        private void ButtonAddRepresentative_Click(object sender, RoutedEventArgs e)
        {
            AddUser window = new AddUser(connection, "Representative");
            window.Closed += delegate { UpdateRepresentativesComboBox(); };
            window.Show();
        }

        private void ButtonAddShop_Click(object sender, RoutedEventArgs e)
        {
            AddShop window = new AddShop(connection);
            window.Show();
        }

        private void ButtonAddSchedule_Click(object sender, RoutedEventArgs e)
        {
            AddSchedule window = new AddSchedule(connection);
            window.Show();
        }

        private void ButtonAddTask_Click(object sender, RoutedEventArgs e)
        {
            AddTask window = new AddTask(connection);
            window.Closed += delegate { UpdateTopicsComboBox(); };
            window.Show();
        }



        private void ButtonShowUsers_Click(object sender, RoutedEventArgs e)
        {
            isRawTable = true;
            DrawTableBySQLCommand("SELECT * FROM users", "users");
        }

        private void ButtonShowSupervisors_Click(object sender, RoutedEventArgs e)
        {
            CopyIfRawTableCheckBox();
            if (isRawTable)
            {
                DrawTableBySQLCommand("SELECT * FROM supervisors;", "supervisors");
            }
            else
            {
                DrawTableBySQLCommand("SELECT supervisors.supervisor_id, users.first_name, users.last_name, users.city, users.phone_number " +
                                      "FROM supervisors " +
                                      "JOIN users ON users.user_id = supervisors.user_id;", "supervisors joined");
            }
            
        }

        private void ButtonShowRepresentatives_Click(object sender, RoutedEventArgs e)
        {
            CopyIfRawTableCheckBox();
            if (isRawTable)
            {
                DrawTableBySQLCommand("SELECT * FROM representatives;", "representatives");
            }
            else
            {
                DrawTableBySQLCommand("SELECT representatives.representative_id, users.first_name, users.last_name, users.city, users.phone_number " +
                                      "FROM representatives JOIN users ON users.user_id = representatives.user_id;", "representatives joined");
            }
            
        }

        private void ButtonShowShops_Click(object sender, RoutedEventArgs e)
        {
            CopyIfRawTableCheckBox();
            if (isRawTable)
            {
                DrawTableBySQLCommand("SELECT * FROM shops;", "shops");
            }
            else
            {
                DrawTableBySQLCommand("SELECT shops.name, shops.address, representatives.representative_id, users.first_name representative_first_name, users.last_name representative_last_name " +
                                      "FROM shops " +
                                      "JOIN representatives ON representatives.representative_id = shops.representative_id " +
                                      "JOIN users ON users.user_id = representatives.user_id;", "shops joined");
            }
        }

        private void ButtonShowSchedules_Click(object sender, RoutedEventArgs e)
        {
            CopyIfRawTableCheckBox();
            if (isRawTable)
            {
                DrawTableBySQLCommand("SELECT * FROM schedules", "schedules");
            }
            else
            {
                DrawTableBySQLCommand("SELECT schedules.shop_id, shops.name, shops.address, schedules.days, schedules.time " +
                                      "FROM schedules " +
                                      "JOIN shops ON shops.shop_id = schedules.shop_id;", "schedules joined");
            }

        }

        private void ButtonShowTasks_Click(object sender, RoutedEventArgs e)
        {
            CopyIfRawTableCheckBox();
            if (isRawTable)
            {
                DrawTableBySQLCommand("SELECT * FROM tasks", "tasks");
            }
            else
            {
                DrawTableBySQLCommand("SELECT tasks.topic, tasks.is_important, " +
                                      "tasks.assigner_id, users_assigners.first_name assigner_first_name, users_assigners.last_name assigner_last_name, " +
                                      "tasks.executor_id, users_executors.first_name executor_first_name, users_executors.last_name executor_last_name, " +
                                      "tasks.shop_id, shops.name, shops.address, tasks.complition_comment " +
                                      "FROM tasks " +
                                      "JOIN supervisors ON tasks.assigner_id = supervisors.supervisor_id " +
                                      "JOIN users users_assigners ON supervisors.user_id = users_assigners.user_id " +
                                      "JOIN representatives ON tasks.executor_id = representatives.representative_id " +
                                      "JOIN users users_executors ON representatives.user_id = users_executors.user_id " +
                                      "JOIN shops ON shops.shop_id = tasks.shop_id;", "tasks joined");
            }
        }



        private void ButtonRepresentativesOfSupervisor_Click(object sender, RoutedEventArgs e)
        {
            isRawTable = false;
            if (comboBoxSupervisors.Text != "")
            {
                DrawTableBySQLCommand("SELECT users_representatives.* " +
                                      "FROM users users_supervisors " +
                                      "JOIN supervisors " +
                                      "ON supervisors.user_id = users_supervisors.user_id " +
                                      "JOIN representatives " +
                                      "ON representatives.supervisor_id = supervisors.supervisor_id " +
                                      "JOIN users users_representatives " +
                                      "ON representatives.user_id = users_representatives.user_id " +
                                      "WHERE supervisors.supervisor_id = " + (comboBoxSupervisors.SelectedValue as User).id + ";", "Representatives of Supervisor");
            }
            else
            {
                MessageBox.Show(Constants.fieldfillingWarning);
            }
        }

        private void ButtonAssignedShopsWithTopicForRepresentative_Click(object sender, RoutedEventArgs e)
        {
            isRawTable = false;
            if (comboBoxRepresentatives.Text != "" && comboBoxTopics.Text != "")
            {
                DrawTableBySQLCommand("SELECT shops.* " +
                                      "FROM users " +
                                      "JOIN representatives " +
                                      "ON representatives.user_id = users.user_id " +
                                      "JOIN tasks " +
                                      "ON tasks.executor_id = representatives.representative_id " +
                                      "JOIN shops " +
                                      "ON shops.shop_id = tasks.shop_id " +
                                      "WHERE representatives.representative_id = " + (comboBoxRepresentatives.SelectedValue as User).id +
                                      " AND tasks.topic = '" + comboBoxTopics.Text + "';", "Shops of Representative of task topic");
            }
            else
            {
                MessageBox.Show(Constants.fieldfillingWarning);
            }
        }

        private void ButtonRepresentativesWithNoShops_Click(object sender, RoutedEventArgs e)
        {
            isRawTable = false;
            DrawTableBySQLCommand("SELECT " +
                                  "users_representatives.first_name representative_first_name, " +
                                  "users_representatives.last_name representative_last_name, " +
                                  "users_supervisors.first_name supervisor_first_name, " +
                                  "users_supervisors.last_name supervisor_last_name " +
                                  "FROM representatives " +
                                  "JOIN supervisors " +
                                  "ON supervisors.supervisor_id = representatives.supervisor_id " +
                                  "JOIN users users_representatives " +
                                  "ON users_representatives.user_id = representatives.user_id " +
                                  "JOIN users users_supervisors " +
                                  "ON users_supervisors.user_id = supervisors.user_id " +
                                  "LEFT JOIN shops " +
                                  "ON shops.representative_id = representatives.representative_id " +
                                  "WHERE shops.representative_id IS NULL;", "Representatives without assigned shops");
        }



        private void ButtonUpdateData_Click(object sender, RoutedEventArgs e)
        {
            if (isRawTable)
            {
                try
                {
                    SqlCommandBuilder builder = new SqlCommandBuilder(dataAdapter);
                    dataAdapter.UpdateCommand = builder.GetUpdateCommand();
                    dataAdapter.Update(dataSet, showedTable);

                    UpdateSupervisorsComboBox();
                    UpdateRepresentativesComboBox();
                    UpdateTopicsComboBox();

                    MessageBox.Show(Constants.successfullQueryMessage);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("You can update data only in raw tables.");
            }
        }



        private void UpdateSupervisorsComboBox()
        {
            SqlCommand command = new SqlCommand("SELECT users.first_name, users.last_name, supervisors.supervisor_id FROM supervisors JOIN users ON users.user_id = supervisors.user_id", connection);
            try
            {
                List<User> users = new List<User>();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    users.Add(new User(reader["first_name"].ToString(), reader["last_name"].ToString(), int.Parse(reader["supervisor_id"].ToString())));
                }
                reader.Close();

                for (int i = 0; i < users.Count; i++)
                {
                    comboBoxSupervisors.Items.Add(users[i]);
                }
                if (users.Count > 0)
                {
                    comboBoxSupervisors.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void UpdateRepresentativesComboBox()
        {
            SqlCommand command = new SqlCommand("SELECT users.first_name, users.last_name, representatives.representative_id FROM representatives JOIN users ON users.user_id = representatives.user_id", connection);
            try
            {
                List<User> users = new List<User>();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    users.Add(new User(reader["first_name"].ToString(), reader["last_name"].ToString(), int.Parse(reader["representative_id"].ToString())));
                }
                reader.Close();

                for (int i = 0; i < users.Count; i++)
                {
                    comboBoxRepresentatives.Items.Add(users[i]);
                }
                if (users.Count > 0)
                {
                    comboBoxRepresentatives.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void UpdateTopicsComboBox()
        {
            SqlCommand command = new SqlCommand("SELECT DISTINCT topic FROM tasks", connection);
            try
            {
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    comboBoxTopics.Items.Add(reader["topic"]);
                }
                reader.Close();

                if (comboBoxTopics.Items.Count > 0)
                {
                    comboBoxTopics.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CopyIfRawTableCheckBox()
        {
            if (checkBoxShowRawTables.IsChecked ?? true)
            {
                isRawTable = true;
            }
            else if (checkBoxShowRawTables.IsChecked ?? false)
            {
                isRawTable = false;
            }
            else
            {
                isRawTable = false;
            }
        }

        private void DrawTableBySQLCommand(string command, string tableName)
        {
            dataAdapter = new SqlDataAdapter(command, connection);
            try
            {
                dataSet = new DataSet();
                dataAdapter.Fill(dataSet, tableName);
                dataGrid.ItemsSource = new DataView(dataSet.Tables[0]);
                dataGrid.DataContext = dataSet;
                showedTable = tableName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
