﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows;

namespace Lab5.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddSchedule.xaml
    /// </summary>
    public partial class AddSchedule : Window
    {
        private SqlConnection connection;

        public AddSchedule(SqlConnection connection)
        {
            this.connection = connection;
            InitializeComponent();
        }

        private void WindowAddSchedule_Loaded(object sender, RoutedEventArgs e)
        {
            SqlCommand command = new SqlCommand("SELECT shops.shop_id, shops.address, shops.name FROM shops;", connection);
            try
            {
                List<Shop> users = new List<Shop>();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    users.Add(new Shop(reader["name"].ToString(), reader["address"].ToString(), int.Parse(reader["shop_id"].ToString())));
                }
                reader.Close();

                for (int i = 0; i < users.Count; i++)
                {
                    comboBoxShops.Items.Add(users[i]);
                }
                if (users.Count > 0)
                {
                    comboBoxShops.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonAddSchedule_Click(object sender, RoutedEventArgs e)
        {
            if (comboBoxShops.Text != "" && textBoxDays.Text != "" && textBoxTime.Text != "")
            {
                SqlCommand command = new SqlCommand("INSERT INTO schedules (shop_id, days, time)" +
                                                    "VALUES (@shopID, @days, @time);", connection);
                command.Parameters.AddWithValue("@shopID", (comboBoxShops.SelectedItem as Shop).id);
                command.Parameters.AddWithValue("@days", textBoxDays.Text);
                command.Parameters.AddWithValue("@time", textBoxTime.Text);
                try
                {
                    command.ExecuteNonQuery();

                    MessageBox.Show(Constants.successfullQueryMessage);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(Constants.fieldfillingWarning);
            }
        }
    }
}
